﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class Game : MonoBehaviour
{
    [Header("Fading")]
    public float fadeSpeed = .1f;
    public float fadePenalty = .1f;
    public float fadeReward = .1f;

    [Header("Game Objects")]
    public GameObject rockPrefab;
    public float[] rockSizes;
    public float rockSpawnTime = 1f;
    public GameObject lightSourcePrefab;
    public float lightSourceSpawnTime = 1.2f;
    public float marginFromWall = .2f;

    [Header("Walls")]
    public GameObject leftWall;
    public GameObject rightWall;
    public float fallSpeed = 1f;

    [Header("UI")]
    public Text lightCountText;
    public Text rockCountText;
    public Image fadeImage;
    public GameObject newGameButton;
    public Text scoreText;

    public enum GameState { Stopped, Running, End}
    private GameState currentState = GameState.Stopped;
    
    private int wallCount = 0;
    private List<GameObject> leftWalls;
    private List<GameObject> rightWalls;
    private Vector3 wallLeftStartPos;
    private Vector3 wallLeftEndPos;
    private Vector3 wallRightStartPos;
    private Vector3 wallRightEndPos;
    private float lightTimer = 0f;
    private float rockTimer = 0f;
    private Transform lightSourceParent;
    private Transform rockParent;
    private int lightCount = 0;
    private int rockCount = 0;
    private float wallWidth;
    private float wallHeight;
    private float fadeCounter = 0f;

    public GameState CurrentState
    {
        get
        {
            return currentState;
        }

        set
        {
            currentState = value;
        }
    }

    void Awake()
    {
        currentState = GameState.Stopped;

        if (newGameButton != null)
            newGameButton.SetActive(false);
        if (scoreText != null)
            scoreText.gameObject.SetActive(false);

        if (rockPrefab == null || lightSourcePrefab == null || rockSizes.Length <= 0)
            enabled = false;

        leftWalls = new List<GameObject>();
        rightWalls = new List<GameObject>();

        lightSourceParent = new GameObject("LightSourceParent").transform;
        rockParent = new GameObject("RockParent").transform;

        Player player = FindObjectOfType<Player>();
        player.Init(this);

        SetUpWalls();
    }

    void Start()
    {
        Application.targetFrameRate = 60;
    }

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
            Application.Quit();

        if (currentState != GameState.Running) return;

        lightTimer += Time.deltaTime;
        rockTimer += Time.deltaTime;

        MoveWalls(Time.deltaTime);

        if (lightTimer >= lightSourceSpawnTime)
        {
            SpawnLight();
        }

        if (rockTimer >= rockSpawnTime)
        {
            SpawnRock();
        }

        // Fading
        if (fadeCounter <= 0f) fadeCounter = 0f;
        fadeCounter += fadeSpeed * Time.deltaTime;

        if (fadeImage != null)
            fadeImage.color = new Color(fadeImage.color.r, fadeImage.color.g, fadeImage.color.b, fadeCounter);

        if (fadeCounter >= 1f)
            EndGame();
    }

    public void StartNewGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    public void EndGame()
    {
        currentState = GameState.End;
        scoreText.gameObject.SetActive(true);
        scoreText.text = "Score: " + lightCount;
        newGameButton.SetActive(true);
    }

    public void LightSourceHit()
    {
        lightCount++;

        //if (lightCountText != null)
        //    lightCountText.text = "Lights: " + lightCount;

        fadeCounter -= fadeReward;
    }

    public void RockHit()
    {
        rockCount++;

        //if (rockCountText != null)
        //    rockCountText.text = "Rocks: " + rockCount;

        fadeCounter += fadePenalty;
    }

    private void SpawnLight()
    {
        float posX = Random.Range(wallLeftStartPos.x + marginFromWall, wallRightStartPos.x - marginFromWall);
        float posY = wallLeftStartPos.y;
        GameObject light = Instantiate(lightSourcePrefab, new Vector3(posX, posY), Quaternion.identity) as GameObject;
        light.transform.SetParent(lightSourceParent);
        light.GetComponent<LightSource>().Init(this, fallSpeed);

        lightTimer = 0f;
    }

    private void SpawnRock()
    {
        float posX = Random.Range(wallLeftStartPos.x + marginFromWall, wallRightStartPos.x - marginFromWall);
        float posY = wallLeftStartPos.y;
        GameObject rock = Instantiate(rockPrefab, new Vector3(posX, posY), Quaternion.identity) as GameObject;
        rock.GetComponent<Rock>().Init(this, fallSpeed);
        rock.transform.SetParent(rockParent);

        int randIdx = Random.Range(0, rockSizes.Length - 1);
        rock.transform.localScale = new Vector3(rockSizes[randIdx], rockSizes[randIdx]);

        rockTimer = 0f;
    }

    private void MoveWalls(float deltaTime)
    {
        GameObject wall = null;
        for (int i = 0; i < leftWalls.Count; i++)
        {
            wall = leftWalls[i];

            if (wall.transform.position.y > wallLeftEndPos.y)
            {
                // Get lowest wall
                float lowestPosY = Mathf.Infinity;
                for (int j = 0; j < leftWalls.Count; j++)
                {
                    if (leftWalls[j].transform.position.y < lowestPosY)
                        lowestPosY = leftWalls[j].transform.position.y;
                }

                wall.transform.position = new Vector3(wall.transform.position.x, lowestPosY) - Vector3.up * wallHeight;
                wall.GetComponent<SpriteRenderer>().enabled = true;
            }

            wall.transform.position += Vector3.up * fallSpeed * deltaTime;
        }

        for (int i = 0; i < rightWalls.Count; i++)
        {
            wall = rightWalls[i];

            if (wall.transform.position.y > wallLeftEndPos.y)
            {
                // Get lowest wall
                float lowestPosY = Mathf.Infinity;
                for (int j = 0; j < rightWalls.Count; j++)
                {
                    if (rightWalls[j].transform.position.y < lowestPosY)
                        lowestPosY = rightWalls[j].transform.position.y;
                }

                wall.transform.position = new Vector3(wall.transform.position.x, lowestPosY) - Vector3.up * wallHeight;
                wall.GetComponent<SpriteRenderer>().enabled = true;
            }

            wall.transform.position += Vector3.up * fallSpeed * deltaTime;
        }
    }

    private void SetUpWalls()
    {
        if (leftWall == null || rightWall == null)
        {
            Debug.Log("Game::SetUpWalls() - Walls not found!");
            enabled = false;
        }

        GameObject wallParent = new GameObject("WallParent");

        wallWidth = leftWall.GetComponent<SpriteRenderer>().bounds.size.x;
        wallHeight = leftWall.GetComponent<SpriteRenderer>().bounds.size.y;

        Vector3 screenBottomLeft = Camera.main.ScreenToWorldPoint(new Vector3());
        Vector3 screenTopLeft = Camera.main.ScreenToWorldPoint(new Vector3(0f, Camera.main.pixelHeight));
        float screenHeight = Vector3.Distance(screenBottomLeft, screenTopLeft);

        wallCount = (int)(screenHeight / wallHeight) + 4;

        float screenLeftPosX = Camera.main.ScreenToWorldPoint(new Vector3(0f, Camera.main.pixelHeight / 2f)).x;
        float screenRightPosX = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, Camera.main.pixelHeight / 2f)).x;

        screenLeftPosX += wallWidth / 2f;
        float screenLeftPosZ = 0f;
        screenRightPosX -= wallWidth / 2f;
        float screenRightPosZ = 0f;

        wallLeftStartPos = new Vector3(screenLeftPosX, -wallHeight * (wallCount / 2f), screenLeftPosZ);
        wallLeftEndPos = new Vector3(screenLeftPosX, wallHeight * (wallCount / 2f), screenLeftPosZ);
        wallRightStartPos = new Vector3(screenRightPosX, -wallHeight * (wallCount / 2f), screenRightPosZ);
        wallRightEndPos = new Vector3(screenRightPosX, wallHeight * (wallCount / 2f), screenRightPosZ);

        GameObject wall = null;
        for (int i = 0; i < wallCount; i++)
        {
            // Left walls
            wall = Instantiate(leftWall);
            wall.transform.SetParent(wallParent.transform);
            wall.transform.localPosition = Vector3.zero;
            wall.transform.localScale = leftWall.transform.localScale;

            wall.transform.position = wallLeftStartPos + Vector3.up * i * wallHeight;

            if (i >= wallCount / 2 + 2)
                wall.GetComponent<SpriteRenderer>().enabled = false;

            leftWalls.Add(wall);

            // Right walls
            wall = Instantiate(rightWall);
            wall.transform.SetParent(wallParent.transform);
            wall.transform.localPosition = Vector3.zero;
            wall.transform.localScale = rightWall.transform.localScale;

            wall.transform.position = wallRightStartPos + Vector3.up * i * wallHeight;

            if (i >= wallCount / 2 + 2)
                wall.GetComponent<SpriteRenderer>().enabled = false;

            rightWalls.Add(wall);
        }

        leftWall.GetComponent<SpriteRenderer>().enabled = false;
        leftWall.transform.position = new Vector3(screenLeftPosX, 0f, 0f);
        BoxCollider2D leftWallCollider = leftWall.AddComponent<BoxCollider2D>();
        leftWallCollider.size = new Vector2(leftWall.GetComponent<SpriteRenderer>().bounds.size.x, wallHeight * wallCount);

        rightWall.GetComponent<SpriteRenderer>().enabled = false;
        rightWall.transform.position = new Vector3(screenRightPosX, 0f, 0f);
        BoxCollider2D rightWallCollider = rightWall.AddComponent<BoxCollider2D>();
        rightWallCollider.size = new Vector2(rightWall.GetComponent<SpriteRenderer>().bounds.size.x, wallHeight * wallCount);
    }
}
