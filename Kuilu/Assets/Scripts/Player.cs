﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Rigidbody2D))]
public class Player : MonoBehaviour
{
    public float moveSpeed = 1f;

    private float input = 0f;
    private Game game;
    private Rigidbody2D rb;

    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    public void Init(Game _game)
    {
        game = _game;
    }

    public void LeftButtonPressed()
    {
        Debug.Log("LeftButtonPressed");
    }

    public void RightButtonPressed()
    {
        Debug.Log("RightButtonPressed");
    }

    void FixedUpdate()
    {
#if UNITY_EDITOR
        input = Input.GetAxis("Horizontal");
#elif UNITY_ANDROID || UNITY_IOS
        if (Input.touchCount == 1)
        {
            Touch touch = Input.GetTouch(0);
            if (touch.position.x < Screen.width / 2f)
                input = -1f;
            else if (touch.position.x > Screen.width / 2f)
                input = 1f;
            else
                input = 0f;
        }
        else
        {
            input = 0f;
        }
#endif

        if (input == 0f)
        {
            return;
        }
        else if (input < -.1f)
        {
            if (game.CurrentState == Game.GameState.Stopped)
                game.CurrentState = Game.GameState.Running;

            MoveLeft(Time.deltaTime);
        }
        else if (input > .1f)
        {
            if (game.CurrentState == Game.GameState.Stopped)
                game.CurrentState = Game.GameState.Running;

            MoveRight(Time.deltaTime);
        }
    }

    void MoveLeft(float deltaTime)
    {
        rb.MovePosition(transform.position + Vector3.left * moveSpeed * deltaTime);
        //transform.position += Vector3.left * moveSpeed * deltaTime;
    }

    void MoveRight(float deltaTime)
    {
        rb.MovePosition(transform.position + Vector3.right * moveSpeed * deltaTime);
        //transform.position += Vector3.right * moveSpeed * deltaTime;
    }
}
