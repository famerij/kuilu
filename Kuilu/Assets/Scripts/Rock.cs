﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour
{
    private Game game;
    private float speed;
    //private Rigidbody2D rb;

    void Awake()
    {
        //rb = GetComponent<Rigidbody2D>();
    }

    public void Init(Game _game, float _speed)
    {
        game = _game;
        speed = _speed;
    }

    void Update()
    {
        if (game.CurrentState != Game.GameState.Running) return;
        //rb.MovePosition(transform.position + Vector3.up * speed * Time.deltaTime);
        transform.position += Vector3.up * speed * Time.deltaTime;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.GetComponent<Player>())
        {
            game.RockHit();

            Destroy(gameObject);
            return;
        }
    }
}
